"""@Author: Rayane AMROUCHE

Main script.
"""

import io
import os
import random
import discord  # type: ignore

from PIL import Image  # type: ignore
from discord.ext import commands  # type: ignore
from dotenv import dotenv_values


from dsmanager import DataManager  # type: ignore

dm = DataManager()
dm.utils.load_env(".env")
config = dotenv_values(".env")


def screenshot(url, token, crop=(100, 430, 1180, 1400)):
    """return screenshot"""
    img_online = dm.get_data(
        "screenshot",
        reload=True,
        formatting={"token": token, "url": url},
        data_type="file",
    )
    return Image.open(img_online).crop(crop)



def change_url(old, new):
    """Change deeplol url"""
    if new == "":
        return 0
    txt = config["url_screenshot"]
    old = old.replace(" ", "%2520")
    new = new.replace(" ", "%2520")
    if txt.find(new) > 0:
        return 0
    config["url_screenshot"] = txt.replace(old, new)
    return 1

def add_url(new):
    """add user to deeplol url"""
    txt = config["url_screenshot"]
    new = new.replace(" ", "%2520")
    if txt.find(new) > 0:
        return 0
    config["url_screenshot"] = txt + "%2C" + new
    return 1

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(intents=intents, command_prefix="!")


@bot.command()
async def rank(ctx):
    if ctx.channel.name != "the-grind":
        return
    image_ = screenshot(
        config["url_screenshot"],
        config["token_screenshot"],
        (80, 270, 1185, 1065),
    )
    bytes_ = io.BytesIO()
    image_.save(bytes_, "PNG")
    bytes_.seek(0)
    await ctx.channel.send(file=discord.File(bytes_, filename="image.png"))


@bot.command()
async def lp_graph(ctx):
    if ctx.channel.name != "the-grind":
        return
    image_ = screenshot(
        config["url_screenshot"],
        config["token_screenshot"],
        (90, 1120, 1210, 1600),
    )
    bytes_ = io.BytesIO()
    image_.save(bytes_, "PNG")
    bytes_.seek(0)
    await ctx.channel.send(file=discord.File(bytes_, filename="image.png"))


# @bot.command()
# async def add(ctx, new):
#     if ctx.channel.name != "the-grind":
#         return
#     add_url(new)
#     await ctx.channel.send(f"Add {new}.")

@bot.command()
async def rename(ctx, old, new):
    if ctx.channel.name != "the-grind":
        return
    res = change_url(old, new)
    if res == 0:
        await ctx.channel.send("Va te faire ptite merde de romain de mes deux.")
    await ctx.channel.send(f"Renamed {old} to {new}.")


@bot.command()
async def avis(ctx):
    if ctx.channel.name != "the-grind":
        return
    msg = random.choice(
        [
            # "Personne ne l'aime ce chien.",
            ":ignorege:",
            # "Arrêtez de me contacter monsieur ça devient agaçant.",
            # "Trop le goat ce boug (la définition péjorative de goat, pas la classique).",
            # "J'ai fait le veux de chasteté, il a fait le choix d'être une merde. Bizarre mais on respecte.",
            # "Sah y'a du monde au balcon, LE BALCON DE LA HONTE !!! (ça veut rien dire, juste je l'aime pas sah)",
            # "Mdr, à bon entendeur.",
            # "Il pu sah.",
            # "If I speak.",
            # "Une merde, en gros.",
            # "Jt'en pose des questions moi (je l'aime pas).",
            # "Si j'avais la possibilité de me débarasser de ce dechet je l'aurais dejà fait.",
            # "Oh nan pas lui... Azy frr tu sais bien que personne l'aime.",
            # "Imo, si tu me poses la question et que tu m'autorise à donner mon avis, humble sois disant passant. Eh bien, dans ce cas là bien précis et aucun autre, j'aurais tendance à te dire que c'est une merde.",
            # "Mon empire contre sa disparition de la surface de la Terre.",
            # "Envie de vomir quand tu parles de lui sah.",
            # "Un bon gars (je mens).",
            # "Si j'avais le choix je serais meme pas sur ce serveur à le cotoyer.",
            # "Me parles pas de cette baltringue. Tu m'as tendu là.",
            # "Qui ? Le connard là ?",
            # "🤮",
        ]
    )
    await ctx.channel.send(msg)


bot.run(config["bot_token"])
